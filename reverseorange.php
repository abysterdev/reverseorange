<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Reverseorange extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'reverseorange';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'AByster Consulting.';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Reverse payment Orange Mobile money');
        $this->description = $this->l('Use to reverse payment on Orange mobile money');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        //Configuration::updateValue('REVERSEMTN_LIVE_MODE', false);

        Configuration::updateValue('RV_ORG_CURRENCY', 'XAF');

        Configuration::updateValue('RV_ORG_PAYMENT_URL', 'https://api.orange.com/orange-money-webpay/cm/v1/webpayment');

        Configuration::updateValue('RV_ORG_STATUS_URL', 'https://api.orange.com/orange-money-webpay/cm/v1/transactionstatus');

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('actionSendPaymentOrange');
    }

    public function uninstall()
    {
        Configuration::deleteByName('JMARKETPLACE_MOBILEMONEY_PHONE_NUMER');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitReverseOrangeModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        //$output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitReverseOrangeModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings Orange mobile'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Sender Orange phone number'),
                        'prefix' => '<i class="icon icon-phone"></i>',
                        'name' => 'JMARKETPLACE_MOBILEMONEY_PHONE_NUMER_ORANGE',
                        'desc' => $this->l('This phone number is used to send Orange mobile payment.'),
                        'required' => true,
                        'lang' => false,
                        'col' => 3,
                    ),

                ),

                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );

    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(

            'JMARKETPLACE_MOBILEMONEY_PHONE_NUMER_ORANGE' => Configuration::get('JMARKETPLACE_MOBILEMONEY_PHONE_NUMER_ORANGE'),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    /**
    * Action to validate Orange mobile money
    * params = order_amount, order_reference, receiver_params
    * returns :
    */

    //hookActionSendPaymentOrange
    public function hookActionSendPaymentOrange($params) {

        $this->ablogorg('Orange mobile money payment begin');

        $order_amount = $params['order_amount'];
        $order_reference = $params['order_reference'];
        $receiver_params = $params['receiver_params'];
        $sender_phone = Configuration::get('JMARKETPLACE_MOBILEMONEY_PHONE_NUMER');

        //$seller_commission_history = new SellerCommissionHistory($id_seller_commission_history);
        //$id_seller = $seller_commission_history->id_seller;

        /*$authorized = false;

        foreach (Module::getPaymentModules() as $module)
        {
            if ($module['name'] == 'orangemoneycmr') {
                $authorized = true;
                break;
            }
        }

        if (!$authorized)
            die($this->module->l('This orange payment method is not available.', 'validation'));

        $this->ablogorg('Orange mobile money payment authorized','INFO');*/

        if ($sender_phone == '')
        {
            $this->ablogorg('Sender Phone number customer invalid.','INFO');

            $ret = array(
                'code' => 0,
                'desc' => 'Sender phone number customer invalid or empty.'
            );
            return $ret;
        }

        //$order = new Order((int)$seller_commission_history->id_order);

        //$infos_commission = $seller_commission_history->getCommissionHistory((int)$this->context->language->id, (int)$this->context->shop->id);

        //$payments = SellerPayment::getActivePaymentsBySeller($id_seller);
        $params = explode(';', $receiver_params);
        if(count($params <= 1)) {
            return false;
        }

        //$total = (float)$infos_commission[0]['total_commission_tax_incl'];

        $infos_order = array(
            'reference' => $order_reference,
            'total_paid' => $order_amount,
        );

        $this->ablogorg('begin load parameters','INFO');

        $this->language = $this->context->language->iso_code;
        $this->currency = Configuration::get('RV_ORG_CURRENCY');
        //$this->base_link = $this->context->link->getPageLink('index', true);
        $this->merchantName = $params[0];
        $this->merchantKey = $params[1];
        $this->authHeader = $params[2];
        $this->tokenUrl = Configuration::get('RV_ORG_TOKEN_URL');
        $this->accessToken = $payments['token'];

        $this->ablogorg('Parameters loading : ','INFO');
        $this->ablogorg('Parameters merchantName : '. $this->merchantName,'INFO');
        $this->ablogorg('Parameters merchantKey : '. $this->merchantKey,'INFO');
        $this->ablogorg('Parameters authHeader : '. $this->authHeader,'INFO');
        $this->ablogorg('Parameters tokenUrl : '. $this->tokenUrl,'INFO');
        $this->ablogorg('Parameters accessToken : '. $this->accessToken,'INFO');

        if(empty($this->accessToken))
        {
            $this->ablogorg('accessToken empty begin generate new token','INFO');

            $token = $this->generateOrgAccessToken();
            if (!$token) {

                $this->ablogorg('Payment method error : no access token provided and generating new one failed. Payment cancelled. ', 'ERROR');
                $ret = array(
                    'code' => 0,
                    'desc' => 'No access token provided and generating new one failed. Payment cancelled.'
                );

                return $ret;

            } else {

                //SellerPayment::setTokenPayment($id_seller,$payments['id_seller_payment'],$token);
                $this->accessToken = $token;
                $this->ablogorg('Token created confirmations: ' . $token, 'INFO');

            }


        }

        $customer_details = array(

            'customer_name' => Configuration::get('PS_SHOP_NAME'),

            'customer_email' => Configuration::get('PS_SHOP_EMAIL'),

            'customer_tel' => Configuration::get('JMARKETPLACE_MOBILEMONEY_PHONE_NUMER_ORANGE')

        );

        return $this->sendPaymentResquest($infos_order,$customer_details);
        //{
            //$seller_commission_history = new SellerCommissionHistory($id_seller_commission_history);
            //$seller_commission_history->id_seller_commission_history_state = SellerCommissionHistoryState::getIdByReference('paid');
            //$seller_commission_history->update();


            //$this->confirmations[] = $this->l('Votre paiement a été bien envoyé.');
            //$ret = array(
            //        'code' => 1,
            //        'desc' => 'Votre paiement a été bien envoyé.'
            //);



        //}


    }

    public function generateOrgAccessToken()
    {
        $header = $this->setRequestHeader('access_token_request');
        if (!$header){
            $this->ablogorg('Token header: ' . $header, 'INFO');
            return false;
        }

        //$this->ablogorg('Token header: ' . $header, 'INFO');

        $data_string = $this->setRequestBody('access_token_request');
        if (!$data_string){
            return false;
        }

        $results = $this->sendRequest($header, $data_string, $this->tokenUrl);
        if (!$results) {
            return false;
        }

        $this->ablogorg('Access Token: ' . $results['access_token'], 'INFO');

        if ($results['access_token']) {

            //Configuration::updateValue('AB_ACCESS_TOKEN', $results['access_token']);
            return $results['access_token'];

        } else {
            return false;
        }
    }

    public function setRequestHeader($operation)
    {

        $header=array();

        switch ($operation) {
            case 'payment_or_status_request':
                if (empty($this->accessToken)) {
                    return false;
                }
                $header[]="Content-Type:application/json";
                $header[]="Accept:application/json";
                $header[]="Authorization:Bearer ".$this->accessToken;
                return $header;

            case 'access_token_request':
                if (empty($this->authHeader)) {
                    $this->ablogorg('Setting access token request header failed because no auth header was provided ', 'ERROR');
                    return false;
                }
                $header[]="Content-Type: application/x-www-form-urlencoded";
                $header[]="Authorization:".$this->authHeader;
                return $header;

            default:
                return false;
        }
    }

    /**
     * Sets request body
     */
    public function setRequestBody($operation, $infos_order = null, $payment_details = null)
    {

        switch ($operation) {
            case 'payment_request':
                $data = array();
                $data['merchant_key'] = $this->merchantKey;
                $data['currency'] = $this->currency;
                $data['order_id'] = $infos_order['reference'];
                $data['amount'] = (float)$infos_order['total_paid'];
                $data['return_url'] = 'return_url';//.'?fc=module&module=orangemoneycmr&controller=finalisation&key=r'.$order->id;
                $data['cancel_url'] = 'cancel_url';//.'?fc=module&module=orangemoneycmr&controller=finalisation&key=c'.$order->id;
                $data['notif_url'] = 'notif_url';//.'?fc=module&module=orangemoneycmr&controller=finalisation&key=n'.$order->id;
                $data['lang'] = $this->language;
                $data['reference'] = $this->merchantName;
                return json_encode($data);

            case 'status_request':
                $data = array();
                $data['order_id'] = $payment_details->ref_order;
                $data['amount'] = $payment_details->payment_amount;
                $data['pay_token'] = $payment_details->pay_token;
                return json_encode($data);

            case 'access_token_request':
                $data = "grant_type=client_credentials";
                return $data;

            default:
                return false;
        }
    }

    /**
     * Send cURL request
     */
    public function sendRequest($header, $data_string, $url){

        $this->ablogorg('Request header '.json_encode($header), 'INFO');
        $this->ablogorg('Request body '.json_encode($data_string), 'INFO');
        $this->ablogorg('Request URL '.$url, 'INFO');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        $result_response = curl_exec($ch);

        $results = array();

        if (curl_errno($ch) > 0) {
            $this->ablogorg('There was a cURL error: '. curl_error($ch), 'ERROR');
            curl_close($ch);
            return false;
        } else {
            $this->ablogorg('Request response'.$result_response, 'INFO');
            $results = json_decode($result_response, true);
            curl_close($ch);

            if ($results){
                return $results;
            } else {
                $this->ablogorg('Decoding JSON failed with the following message: '.json_last_error_msg(), 'ERROR');
                return false;
            }
        }

    }

    public function sendPaymentResquest($infos_order, $customer_details)
    {

        $this->ablogorg('Initiating payment request ...', 'INFO');
        $this->ablogorg('Setting payment request header ...', 'INFO');
        $header = $this->setRequestHeader('payment_or_status_request');
        if (!$header){
            $this->ablogorg('Setting payment request header failed', 'ERROR');
            //$this->errors[] = Tools::displayError('Setting payment request header failed', 'AdminSellerCommissionsHistoryController');
            $ret = array(
                    'code' => 0,
                    'desc' => 'Setting payment request header failed'
            );
            return $ret;
        }

        $this->ablogorg('Setting payment request body ...', 'INFO');
        $data_string = $this->setRequestBody('payment_request', $infos_order);
        if (!$data_string){
            $this->ablogorg('Setting payment request body failed', 'ERROR');
            //$this->errors[] = Tools::displayError('Setting payment request body failed', 'AdminSellerCommissionsHistoryController');
            $ret = array(
                    'code' => 0,
                    'desc' => 'Setting payment request body failed'
            );
            return $ret;
        }

        $this->ablogorg('Sending payment request ...', 'INFO');
        $results = $this->sendRequest($header, $data_string, Configuration::get('RV_ORG_PAYMENT_URL'));
        if (!$results) {
            //$this->ablogorg('Sending payment request ...', 'INFO');
            //$this->errors[] = Tools::displayError('Error Sending payment request ...', 'AdminSellerCommissionsHistoryController');
            $ret = array(
                    'code' => 0,
                    'desc' => 'Error Sending payment request'
            );
            return $ret;
        }

        switch ($results['message']) {
            case 'OK':
                $this->ablogorg('Payment request confirmationsfull !!!', 'INFO');
                $this->ablogorg('Saving payment ...', 'INFO');
                //OrangeMoneyCmrPayment::savePayment($order, $results, $customer_details);
                $this->ablogorg('Commission paid', 'INFO');
                $ret = array(
                    'code' => 1,
                    'desc' => 'Votre paiement a été bien envoyé.'
                );
                return $ret;
                //return true;

            case 'Expired credentials':
                $this->ablogorg('Payment request failed with the following message: Expired credentials', 'ERROR');
                $this->ablogorg('Genereting new access token: ', 'INFO');
                $token = $this->generateAccessToken();
                if ($token) {
                    $this->accessToken = $token;
                    $this->sendPaymentResquest($infos_order, $customer_details);
                } else {
                    //$this->errors[] = Tools::displayError('Expired credentials', 'AdminSellerCommissionsHistoryController');
                    $ret = array(
                    'code' => 0,
                    'desc' => 'Expired credentials'
                    );
                    return $ret;
                    //return false;
                }

            default:
                //$this->errors[] = Tools::displayError($results['message']);
                $ret = array(
                    'code' => 0,
                    'desc' => $results['message']
                    );
                return $ret;
        }
    }

    public function ablog($data,$level){

        $day = gmdate("Y-m-d");

        $logfile=dirname(__FILE__).'/../../log/reverseMTN_'.$day.'.log';

        error_log("\r\n".'['.gmdate("Y-m-d H:i:s").'] '.$level.' '.$data, 3, $logfile);

    }

    public function ablogorg($data,$level=null){

        $day = gmdate("Y-m-d");

        $logfile=dirname(__FILE__).'/log/reverseORG_'.$day.'.log';

        error_log("\r\n".'['.gmdate("Y-m-d H:i:s").'] '.$level.' '.$data, 3, $logfile);

    }
}
